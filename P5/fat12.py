import sys, os
import datetime
from struct import unpack
from string import printable
import itertools, collections
from os import path

CODEC = "cp437"

def consume(server, n=None):
  '''Consumes and wastes n elements from a given generator'''
  collections.deque(itertools.islice(server, n), maxlen=0)

# unused
def fields(obj):
  '''Returns all the fields of an object in a simple, human readable format'''
  return "\n".join(f"{(k+':'): <25}{obj.__dict__[k]}" for k in obj.__dict__)

# unused
def fmat_bytes(b, width=8):
  '''Formats a bytestring into regular columns of hex bytes, 
  with the appropriate associated ascii characters on the right side'''
  prset = set(printable) - set("\t\n\r\x0b\x0c")
  ret = ""
  for i in range(0, len(b), width):
    nxt = min(i+width, len(b))
    toadd = " ".join(map(lambda n: ('0' if n < 16 else '') + hex(n)[2:].upper(), b[i:nxt]))
    ret += toadd + " "*(width*3 - len(toadd) + 2)
    ret += "".join(map(lambda n: chr(n) if chr(n) in prset else " ", b[i:nxt]))
    ret += "\n"
  return ret

def fmat_commas(n, *, delim=",", spacing=3):
  '''Formats a large integer by inserting commas'''
  ret = ""
  factor = 10**spacing
  while n >= factor:
    ret = delim + f"{n%factor:0>3}" + ret
    n //= factor
  return str(n) + ret

# unused
def fmat_size(b):
  '''Formats a large data size integer by reducing it to 
  the most relevant denomination and postfixing the appropriate 
  size, up to TB'''
  for i in range(4,0,-1):
    if 1024**i < b:
      return f"{b/1024**i:.2f} {['B', 'KB', 'MB', 'GB', 'TB'][i]}"
  return f"{b}  B"

def fmat_time(t):
  '''Formats a time object to a specific layout'''
  h, m = t.hour if t.hour > 0 else 12, t.minute
  if t.hour > 12:
    return f"{h-12: >2}:{m:0>2}p"
  else:
    return f"{h: >2}:{m:0>2}a"

def unpack_12bit(b1, b2, b3):
  ''' Unpacks 3 8-bit bytes into 2 12-bit fat entries'''
  return (b2 & 0x0f) << 8 | b1, b3 << 4 | (b2 & 0xf0) >> 4

def is_bit_set(byte, check):
  '''Checks if the corresponding bit is set in a given byte'''
  return bool((1 << check) & byte)

class SerialNumber:
  '''A class that represents 4-byte little-endian 
  hexadecimal serial numbers'''
  def __init__(self, byte_str):
    self.bytes = byte_str

  def __str__(self):
    n = "".join(map(lambda b: hex(b)[2:].upper(), self.bytes[::-1]))
    return f"{n[:4]}-{n[4:]}"

  def __repr__(self):
    return str(self)

class DiskHeader:
  '''A class that represents the information stored in the boot sector of a
  fat12 partition; the first 512 bytes'''
  def __init__(self, data):

    if len(data) < 512:
      raise Exception(f"byte list too small {len(byte_list)} < 512)")
    if data[510:512] != b'\x55\xAA':
      raise Exception(f"invalid safety bits ({data[510:512]} != b'\\x55\\xaa'")

    self.oem_name =             data[3:11].decode(CODEC)
    self.bytes_per_sector =     unpack("<H", data[11:13])[0]
    self.sectors_per_cluster =  data[13]
    self.reserved_sectors =     unpack("<H", data[14:16])[0]
    self.fat_copies =           data[16]
    self.root_entries =         unpack("<H", data[17:19])[0]
    self.sectors =              unpack("<H", data[19:21])[0]
    self.media_descriptor =     data[21]
    self.sectors_per_fat =      unpack("<H", data[22:24])[0]
    self.sectors_per_track =    unpack("<H", data[24:26])[0]
    self.sides =                unpack("<H", data[26:28])[0]
    self.hidden_sectors =       unpack("<L", data[28:32])[0]
    self.large_sectors =        unpack("<L", data[32:36])[0]
    self.physical_disk_number = data[36]
    self.signature =            data[38]
    self.serial_number =        SerialNumber(data[39:43])
    self.volume_label =         data[43:54].decode(CODEC)
    self.format_type =          data[54:62].decode(CODEC)

  def __str__(self):
    return fields(self)
  
  def __repr__(self):
    return str(self) 

class Disk:
  '''A class that represents an entire fat12 partition and all of it's files'''
  def __init__(self, param):
    if type(param) == str:
      self.__init_file(param)
    elif type(param) == bytes:
      self.__init_data(param)
    else:
      raise Exception(f"no matching constructor for argument type \"{type(param)}\".")
  
  def __init_file(self, filename):
    try:
      with open(filename, "rb") as fatfile:
        data = fatfile.read()
        self.__init_data(data)
    except FileNotFoundError:
      print(f"file \"{filename}\" not found")

  def __init_data(self, data):
    self.data = data
    self.header = DiskHeader(data[:512])
    self.fat = self.decode_fat(self.get_fat_sector(1))
    self.root = self.get_root_directory()

  def decode_fat(self, sector):
    '''Decodes 12-bit fat entries into a more conventient format'''
    server = self.serve_consecutive_sectors(sector, self.header.sectors_per_fat)
    raw_fat = []
    try:
      while server:
        raw_fat += unpack_12bit(next(server)[1], next(server)[1], next(server)[1])
    except StopIteration:
      pass
    return raw_fat
  
  def consecutive_fat_sectors(self, cluster):
    '''Generates consecutive fat sectors from 
    the fat, starting from the provided cluster'''
    while cluster > 0x001 and cluster < 0xFF7:
      yield self.cluster_to_sector(cluster)
      cluster = self.fat[cluster]

  def serve_from_fat(self, cluster):
    '''Generates address/byte pairs from the clusters of 
    consecutive fat sectors, starting from the given cluster number'''
    for s in self.consecutive_fat_sectors(cluster):
      offset = self.get_sector_position(s)
      for i, b in enumerate(self.get_sector(s)):
        yield i+offset, b
    
  def serve_consecutive_sectors(self, sector, n=None):
    '''Generates address/byte pairs from contiguously allocated
    space, starting from a given logical sector'''
    offset = self.get_sector_position(sector)
    limit = len(self.data) - offset
    if n:
      limit = offset + self.header.bytes_per_sector * n
    while offset < limit:
      yield offset, self.data[offset]
      offset += 1

  def get_sector(self, sector):
    '''Returns the chunk of bytes associated with the given sector'''
    pos = self.get_sector_position(sector)
    return self.data[pos:pos + self.header.bytes_per_sector]

  def get_sector_position(self, sector):
    '''Gets the byte offset of the start of a given sector'''
    return self.header.bytes_per_sector * sector

  def get_directory(self, cluster):
    '''Gets a list of DirectoryEntry objects for all 
    the directories starting from a given cluster'''
    return list(self.__get_directory(self.serve_from_fat(cluster)))

  def get_root_directory(self):
    '''Gets a list of DirectoryEntry objects for all
    files in the root directory'''
    return list(self.__get_directory(self.serve_consecutive_sectors(self.get_root_sector())))

  def get_root_sector(self):
    '''Gets the first sector that the root directory occupies'''
    return self.header.sectors_per_fat * self.header.fat_copies + self.header.reserved_sectors

  def get_fat_sector(self, copy=1):
    '''Gets the first sector that the specified fat copy occupies'''
    if copy > self.header.fat_copies:
      raise Exception(f"Attempted to address fat copy {copy}, when there are only {self.header.fat_copies} copies")
    return self.header.sectors_per_fat * (copy-1) + self.header.reserved_sectors

  def get_first_cluster_sector(self):
    '''Gets the first sector in the data area, 
    equivalent to cluster #2'''
    return self.get_root_sector() + (self.header.root_entries * 32) // self.header.bytes_per_sector

  def cluster_to_sector(self, cluster):
    '''Converts a logical cluster number to its corresponding 
    physical disk sector'''
    return cluster + self.get_first_cluster_sector() - 2
  
  def sector_to_cluster(self, sector):
    '''Converts a physical disk sector to its corresponding 
    logical cluster number'''
    return sector - self.get_first_cluster_sector() + 2

  def visible_root(self):
    '''Returns the subset of the root directory of 
    files that are visible'''
    yield from itertools.takewhile(lambda f:not (f.deleted or f.is_hidden), self.root)

  def __get_directory(self, data_server):
    '''Generates consecutive DirectoryEntry objects 
    for every 32 bytes in a data generator'''
    while data_server:
      addr, byte = next(data_server)
      if byte == 0x0:
        break
      yield DirectoryEntry(self, addr)
      consume(data_server, 31)

  def get_file_data(self, cluster, n_bytes):
    '''Retrieves the bytes of a given file using the fat'''
    return bytes(itertools.islice(map(lambda a: a[1], self.serve_from_fat(cluster)), n_bytes))

  def view_directory(self, dirent, view_hidden=False, view_all=False):
    '''Returns a human readable view of a given list of DirectoryEntry objects'''
    totalsize = 0
    count = 0
    ret = f" Volume Serial Number is {self.header.serial_number}\n\n"
    volume_label = ""
    for entry in dirent:
      if not view_all:
        if entry.deleted or (not view_hidden and entry.is_hidden):
          continue
      if entry.is_volume_label:
        volume_label = entry.file_name
        if not view_all:
          continue
      ret += f"{entry.base_name} {entry.extension}{fmat_commas(entry.file_size): >14} "
      ret += f"{entry.creation_date.strftime('%m-%d-%y')} {fmat_time(entry.creation_time)}\n"
      totalsize += entry.file_size
      count += 1
    ret += f"       {str(count)+' file(s)': <18}{fmat_commas(totalsize)} bytes\n"
    ret = f" Volume name is {volume_label.strip()}\n" + ret
    return ret

  def extract_all_recursive(self, directory, subpath=".", yield_names=False):
    '''Recursively walks the root directory and all subdirectories, and
    extracts all found files to the given directory'''
    if not path.isdir(subpath):
      os.mkdir(subpath)
    for f in directory:
      if f.is_subdirectory and f.format_name() not in [".", ".."]:
        extract_all_recursive(f.get_subdirectory(), path.join(subpath, f.format_name()), yield_names=yield_names)
      else:
        if yield_names:
          yield f.format_name()
        f.write_out(fpath=subpath)

  def view_root(self):
    '''Returns a human readable view of the root directory'''
    return self.view_directory(self.root)

class DirectoryEntry:
  '''A class representing a single file in a fat directory'''
  def __init__(self, disk, offset):
    self.disk = disk
    data = disk.data[offset:offset+32]

    self.deleted =         data[0] == 0xe5
    newdata = list(data)
    if newdata[0] == 0x05:
      newdata[0] = 0xe5
    newdata[:11] = map(lambda x:x%128, newdata[:11])
    data = bytes(newdata)
    
    raw_time = unpack("<H", data[22:24])[0]
    hour =   (raw_time & 0xf800) >> 11
    minute = (raw_time & 0x07e0) >> 5
    second = (raw_time & 0x001f) * 2
    self.creation_time =   datetime.time(hour, minute, second)

    raw_date = unpack("<H", data[24:26])[0]
    year =  ((raw_date & 0xfe00) >> 9) + 1980
    month =  (raw_date & 0x01e0) >> 5
    day =    (raw_date & 0x001f) 
    self.creation_date =   datetime.date(year, month, day)

    self.file_name =       data[:11].decode(CODEC)
    self.base_name =       self.file_name[:8]
    self.extension =       self.file_name[8:11]
    self.is_readonly =     is_bit_set(data[11], 0)
    self.is_hidden =       is_bit_set(data[11], 1)
    self.is_system_file =  is_bit_set(data[11], 2)
    self.is_volume_label = is_bit_set(data[11], 3)
    self.is_subdirectory = is_bit_set(data[11], 4)
    self.should_archive =  is_bit_set(data[11], 5)
    self.data_cluster =    unpack("<H", data[26:28])[0]
    self.file_size =       unpack("<L", data[28:32])[0]

  def __str__(self):
    return fields(self)
  
  def __repr__(self):
    return str(self) 

  def get_subdirectory(self):
    '''If this file represents a subdirectory, then retrieves the list of
    DirectoryEntry objects representing that subdirectory'''
    if self.is_subdirectory:
      return self.disk.get_directory(self.data_cluster)
    else:
      raise Exception("Not a subdirectory")

  def get_data(self):
    '''Retrieves the contiguous data of the file using the fat'''
    return self.disk.get_file_data(self.data_cluster, self.file_size)

  def format_name(self):
    '''Gets a modern formatting of the file's name'''
    if self.extension == "   ":
      return self.base_name.strip()
    else:
      return f"{self.base_name.strip()}.{self.extension}" 

  def write_out(self, fpath="."):
    '''Writes the file's contents to the local system, into the given directory'''
    with open(path.join(fpath, self.format_name()), "wb") as f:
      f.write(self.get_data())

usage = "Usage: fat12.py <filename> <view|extract>" 

def main(args):
  if len(args) < 2:
    print(usage)
    return
  
  args[1] = args[1].lower()
  if args[1] not in ["view", "extract"]:
    print(usage)
    return

  try:
    disk = Disk(args[0])
    if args[1] == "view":
      print(disk.view_root())
    elif args[1] == "extract":
      print(f"Extracting: {len(list(disk.visible_root()))} files/folders")
      subdir = "."
      if len(args) > 2:
        subdir = args[2]
      tot_len = 0
      for name in disk.extract_all_recursive(disk.visible_root(), subpath=subdir, yield_names=True):
        if tot_len > 70:
          print()
          tot_len = 0
        toprint = name + ", "
        print(toprint, end="")
        tot_len += len(toprint)
      print()
  except Exception as e:
    print(e)
    print(usage)

if __name__ == "__main__": main(sys.argv[1:])
