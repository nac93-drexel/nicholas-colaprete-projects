#include <linux/mm.h>
#include <linux/module.h>
#include <linux/nmi.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/highmem.h>
#include <linux/smp_lock.h>
#include <asm/mmu_context.h>
#include <linux/interrupt.h>
#include <linux/capability.h>
#include <linux/completion.h>
#include <linux/kernel_stat.h>
#include <linux/debug_locks.h>
#include <linux/security.h>
#include <linux/notifier.h>
#include <linux/profile.h>
#include <linux/freezer.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/delay.h>
#include <linux/smp.h>
#include <linux/threads.h>
#include <linux/timer.h>
#include <linux/rcupdate.h>
#include <linux/cpu.h>
#include <linux/cpuset.h>
#include <linux/percpu.h>
#include <linux/kthread.h>
#include <linux/seq_file.h>
#include <linux/syscalls.h>
#include <linux/times.h>
#include <linux/tsacct_kern.h>
#include <linux/kprobes.h>
#include <linux/delayacct.h>
#include <linux/reciprocal_div.h>

#include <asm/tlb.h>
#include <asm/unistd.h>

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/list.h>

#define PER_LINE 256

static char MODNAME[] = "timeslices";

/*static int do_proc_test(char* sys_buf, char** my_buf, off_t file_pos, int length)
{
  if (file_pos > 0)
    return 0;
  static char buff[128];
  int len = sprintf(buff, "Destined to fail\n");
  *my_buf = buff;
  return len;
}*/

struct user_info
{
  struct list_head neighbor_users;
  struct list_head processes;
  uid_t uid;
  char* name;
};

struct proc_info
{
  struct list_head neighbor_procs;
  char* name;
  int cputime;
};

/*static int do_proc(char* sys_buf, char** my_buf, off_t file_pos, int length)
{
  if (file_pos > 0)
    return 0;

  // Index all processes by user
  
  int lines = 0;
  struct list_head ulist;
  INIT_LIST_HEAD(&ulist);
  struct task_struct* task;
  for_each_process(task) {
    struct proc_info* pinf = kmalloc(sizeof(struct proc_info), GFP_KERNEL);
    pinf->name = kmalloc(strlen(task->comm), GFP_KERNEL);
    strcpy(pinf->name, task->comm);
    pinf->cputime = task->time_slice;
    lines++;

    bool found = false;
    struct user_info user;
    list_for_each_entry(&user, &ulist, neighbor_users)
    {
      if (user->uid == task->user->uid)
      {
        found = true;
        list_add(&pinf->neighbor_procs, &user->processes);
        break;
      }
    }
    if (!found)
    {
      struct user_info* new_user = kmalloc(sizeof(struct user_info), GFP_KERNEL);
      new_user->uid = task->user->uid;
      new_user->name = "placeholder";
      INIT_LIST_HEAD(&user_info->processes);
      list_add(&pinf->neighbor_procs, &new_user->processes);
      list_add(&new_user->neighbor_users, &ulist);
      lines++;
    }
  }

  // Compile process information into string buffer

  char* buff = kmalloc(lines * PER_LINE, GFP_KERNEL);
  struct user_info* user;
  struct user_info* n;
  list_for_each_entry_safe(user, n, ulist, neighbor_user)
  {
    sprintf(buff, "%s%s, [%d]:\n", buff, user->name, user->uid);
    struct proc_info* pinf;
    struct proc_info* p;
    list_for_each_entry_safe(pinf, p, &user->processes, neighbor_procs)
    {
      sprintf(buff, "%s    %s, %d\n", buff, pinf->name, pinf->cputime);
      list_del(&pinf->neighbor_procs);
      kfree(pinf->name);
      kfree(pinf);
    }
    list_del(&user->neighbor_users);
    //kfree(user->name);
    kfree(user);
  }

  *my_buf = buff;
  return strlen(buff);
}*/

#define MAX_MSG_SIZE 2 << 12

static int do_proc(char* sys_buf, char** my_buf, off_t file_pos, int length)
{
  printk("sys_buf: \"%s\"\n", sys_buf);
  printk("my_buf:\n");
  for (int i = 0; my_buf[i]; i++)
    printk("\"%s\"\n", my_buf[i]);
  printk("file_pos: %li\n", file_pos);
  printk("length: %d\n", length);
  if (file_pos > 0)
    return 0;
  
  char* buff = kcalloc(MAX_MSG_SIZE, 1, GFP_KERNEL);

  struct task_struct* task;
  for_each_process(task)
  {
    sprintf(buff, "%s%-3u %-16s %d\n", buff, task->uid, task->comm, task->time_slice);  
  }
  *my_buf = buff;
  return strlen(buff);
}

int init_module(void)
{
  create_proc_info_entry(MODNAME, S_IFREG | S_IRUGO, &proc_root, do_proc);
  return 0;
}

void cleanup_module(void)
{
  remove_proc_entry(MODNAME, &proc_root);
}
