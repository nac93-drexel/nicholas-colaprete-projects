In this project are included 2 files, a kernel module that can be compiled to add the /proc/timeslices entry, which when read,
displays the remaining time slices of all running processes. Secondly, it includes a patch to the kernel which changes the 
time slice allocation program to automatically divide up cpu time equally between each user, and within each user, divide cpu
time evenly between all processes. To install the kernel module, simply compile cpuproc.c into cpuproc.ko and run the command
`insmod cpuproc.ko` on the desired machine to add the proc entry. You can also run 'git patch project4.patch' to patch in the
equal cpu time change into the scheduler of the kernel source code.
