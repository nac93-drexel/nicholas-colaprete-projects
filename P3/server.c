
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MAXLEN 1024

int main(int argc, char** argv)
{
  char textbuf[MAXLEN];

  printf("Server starting with pid #%d\n", getpid());

  for (;;)
  {
    if (syscall(293, -1, MAXLEN, &textbuf) == 0)
    {
      printf("Recieved message: \"%s\"\n", textbuf);
    }
  }
}
