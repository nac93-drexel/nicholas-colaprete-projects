#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_STR 64

void do_child(int n_siblings)
{
  int* sibling_pids = malloc(sizeof(int)*n_siblings);
  int p_pid = getppid();
  int mypid = getpid();
  printf(" -- %d -- Awaiting pids from parent %d\n", mypid, p_pid);
  
  int rem_siblings = n_siblings;
  char recv_buff[MAX_STR];
  while (rem_siblings > 0)
  {
    if (syscall(293, p_pid, MAX_STR, &recv_buff) != -1)
    {
      printf(" -- %d -- Recieved %s from parent\n", mypid, recv_buff);
      sibling_pids[n_siblings - rem_siblings] = atoi(recv_buff);
      rem_siblings--;
    }
  }

  printf(" -- %d -- All sibling pids retrieved, notifying siblings\n", mypid);
  for (int i = 0; i < n_siblings; i++)
  {
    syscall(292, sibling_pids[i], MAX_STR, "Arrived");
  }

  printf(" -- %d -- Awaiting arrival notification from siblings\n", mypid);
  int rem_arrivals = n_siblings;
  while (rem_arrivals > 0)
  {
    if (syscall(293, sibling_pids[n_siblings - rem_arrivals], MAX_STR, &recv_buff) != -1)
    {
      printf(" -- %d -- Recieved arrival signal from sibling %d: \"%s\"\n", mypid, sibling_pids[n_siblings - rem_arrivals], recv_buff);
      rem_arrivals--;
    }
  }

  printf(" -- %d -- All siblings arrived, exiting\n", mypid);
  free(sibling_pids);
}

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }

  int n_child = atoi(argv[1]);

  printf("My pid is %d\n", getpid());
  printf("Spawning %d children\n", n_child);

  int* child_arr = malloc(sizeof(int)*n_child);
  for (int i = 0; i < n_child; i++)
  {
    int retval = fork();
    if (!retval)
    {
      free(child_arr);
      do_child(n_child - 1);
      return 0;
    }
    else
      child_arr[i] = retval; 
  }

  printf("Spawned children with pids ");
  int j;
  for (j = 0; j < n_child-1; j++)
    printf("%d, ", child_arr[j]);
  printf("%d.\n", child_arr[j]);

  printf("Communicating pids to children\n");
  char pid_str[MAX_STR];
  for (int i = 0; i < n_child; i++)
  {
    for (j = 0; j < n_child; j++)
    {
      if (i == j)
        continue;
      printf("%d -> %d\n", child_arr[j], child_arr[i]);
      sprintf(pid_str, "%d", child_arr[j]);
      syscall(292, child_arr[i], MAX_STR, &pid_str);
    }
  }
  printf("Finshed communicating pids to children\n");
}
