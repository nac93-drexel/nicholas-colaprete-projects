This patch file, project3.patch, will patch in three syscalls to the linux kernel; mysend (#292), myrecieve (#293), and
mysend_block (#294). Also attached are three driver programs, with which one can test the functionality of the syscalls.
A server program, which will run continuously, waiting for messages, a client program, which allows the user to send input from 
stdin to the server program, and a threadbarrier program, which demonstrates the message sharing capability by having a number
of children spawn and all communicate with eachother via messages, before terminating.

To implement these syscalls, I created a custom file kernel/mailbox.c where I implemented the syscalls, as well as modifying the
file include/linux/sched.h to add an extra property to the task_struct struct, and implement a new struct to represent mails. 
I also modified all the various files necessary to register the three syscalls in the kernel. 

You can view the repository where this is implemented here: https://bitbucket.org/nac93-drexel/cs-370-linux-kernel/src
