#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MAXLEN 1024

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }

  int serv_pid = atoi(argv[1]);

  char textbuf[MAXLEN];

  printf("Sending messages to process #%d\n", serv_pid);

  for (;;)
  {
    printf("> ");
    for (int i = 0; i < MAXLEN; textbuf[i++] = '\0');
    int read = gets(textbuf, MAXLEN, stdin);
    int retval = syscall(292, serv_pid, MAXLEN, &textbuf);
    if (retval == 0)
      printf("Sent\n");
    else
      printf("Error sending\n");
  }
}
