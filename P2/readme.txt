## Readme for Project 2

I was able to implement 6 out of the 7 required syscalls; 

 * sys_mygetpid
 * sys_steal
 * sys_quad
 * sys_swipe
 * sys_zombify
 * sys_myjoin

I had signficiant difficulty implementing sys_forcewrite, and in the end did not have enough time to finish a working
implementation. Simply use the patch file 'project2.patch' to patch in the additions to the linux kernel. In the folder
testscripts I've included various scripts that can be used to test the functionality of each syscall. There is at least
one program to test each syscall, and various programs for spawning spinning programs to test the functionality of syscalls
which affect running programs.

I wasn't able to provide a screencast demonstrating the scripts, as sandbox.cci.drexel.edu was experiencing frequent outages
on the night of the assignment's due date, and I was unable to record a complete screencast.
