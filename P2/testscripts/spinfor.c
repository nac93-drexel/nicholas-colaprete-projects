#include <stdio.h>
#include <time.h>

int main(int argc, char** argv)
{
  int wait;
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }
  wait = atoi(argv[1]);
  printf("My PID is %i\n", getpid());
  printf("Spinning for %i seconds\n", wait);

  long finish_time = clock() + CLOCKS_PER_SEC * wait;
  while (clock() < finish_time)
  {
    for (int i = 0; i < 100000000; i++) {}
  }
  printf("Done spinning\n");
  return 0;
}
