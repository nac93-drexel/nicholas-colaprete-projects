#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
  char* filename;
  char* texttowrite = "test text";

  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }
  else if (argc >= 2)
    filename = argv[1];
  if (argc > 2)
    texttowrite = argv[2];

  printf("Attempting to force write \"%s\" to %s\n", texttowrite, filename);

  int rval = syscall(291, filename, texttowrite, strlen(texttowrite));
  printf("rval: %i\n", rval);

  if (rval < 0)
  {
    printf("Unable to write to file\n");
    return -1;
  }
  else
  {
    printf("Successfully wrote %i bytes to file\n");
    return 0;
  }
}
