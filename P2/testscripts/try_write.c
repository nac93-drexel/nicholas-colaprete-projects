#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
  char* filename;
  char* texttowrite = "test text";

  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }
  else if (argc >= 2)
    filename = argv[1];
  if (argc > 2)
    texttowrite = argv[2];

  printf("Attempting to write \"%s\" to %s\n", texttowrite, filename);
  
  int fd = open(filename, O_WRONLY);
  if (fd < 0)
  {
    printf("Unable to open file\n");
    return -1;
  }

  int err = write(fd, texttowrite, strlen(texttowrite));
  printf("err: %i\n", err);
  if (err < 0)
  {
    printf("Unable to write to file\n");
    return -1;
  }

  printf("Succesfully wrote %i bytes to file\n", err);
  return 0;
}
