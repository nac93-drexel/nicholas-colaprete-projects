#include <stdio.h>

int main(int argc, const char** argv) 
{
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }
  int pid = atoi(argv[1]);
  printf("Quadrupling process #%i timeslice\n", pid);
  long new_slice = syscall(287, pid);
  if (new_slice == -2)
  {
    printf("Failed to find process\n");
    return -1;
  }
  printf("New timeslice is %li\n", new_slice);
  return 0;
}
