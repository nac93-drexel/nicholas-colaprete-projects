#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv)
{
  int c_count = 3;
  if (argc >= 2)
    c_count = atoi(argv[1]);

  for (int i = 0; i < c_count; i++)
  {
    if (fork() == 0)
    {
      while (1) {}
    }
    else
      printf("Spawned child %i\n", i);
  }
  printf("Looping\n");
  while (1) {}
}
