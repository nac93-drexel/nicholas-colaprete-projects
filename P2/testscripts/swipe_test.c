#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char** argv)
{
  int target = getpid();
  int victim;
  if (argc < 2)
  {
    printf("Not enough args\n");
    return -1;
  }
  else if (argc == 2)
  {
    victim = atoi(argv[1]);
  }
  else
  {
    target = atoi(argv[1]);
    victim = atoi(argv[2]);
  }
  printf("Stealing timeslice from %i and their children\n", victim);
  long amount = syscall(288, target, victim);
  if (amount == -3)
    printf("A process can't steal from itself!\n");
  else if (amount == -2)
    printf("Can't find processes with ids %i and %i\n", target, victim);
  else if (amount == -1)
    printf("Unknown error\n");
  else
  {
    printf("Stole %li time\n", amount);
    return 0;
  }
  return -1;
}
